from typing import List, Dict
from flask import Flask, render_template, request, Response
from flask_mysqldb import MySQL
import time
from datetime import datetime
import mysql.connector


app = Flask(__name__)



#db configuration 
#app.config['MYSQL_HOST'] = 'vcm-20578.vm.duke.edu:6603'
#app.config['MYSQL_USER'] = 'root'
#app.config['MYSQL_PASSWORD'] = 'registrar'
#app.config['MYSQL_DB'] = 'test_db'

#create mysql instance
#mysql = MySQL(app)

#Log time 
#timeStamp = time.time()
#date = datetime.fromtimestamp(timeStamp).strftime("%Y-%m-%d %H:%M:%S")

@app.route('/')
def index():
    return "Index Page"

#webhook     
@app.route('/webhook', methods=['POST'])
def respond():
    #print(request.json);
    #details = request.json
    #question = details["question"]
    #sender = details["sender"]
    #cur = mysql.connection.cursor()
    #cur.execute("INSERT INTO test_table (question, sender) VALUES (%s, %s)", (question, sender))
    #mysql.connection.commit()
    #cur.close()
    #send_email(request.json) #execute email python script 
    
    
    #docker db config
    config = {
        'user': 'root',
        'password': 'registrar',
        'host': 'db',
        'port': '3306',
        'database': 'test_db'
    }
    connection = mysql.connector.connect(**config)
    cursor = connection.cursor()
    cursor.execute('SELECT * FROM test_table')
    results = [{question: sender} for (question, sender) in cursor]
    cursor.close()
    connection.close()    
    return Response(status=200)

@app.route('/hello')
def hello_world():
    return "<p>Hello, World!</p>"

@app.route('/chat-bot')
def chat_bot():
    return render_template('microsoft_template.html')



